/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 13:15:16 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/18 20:46:19 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int			ft_slice(char **line, char **stc)
{
	int				len;

	len = 0;
	if (ft_strchr(*stc, '\n'))
	{
		len = ft_strlenc(*stc, '\n');
		*line = ft_strsub(*stc, 0, (size_t)len);
		*stc = ft_strsub_free(*stc, (unsigned int)len + 1,
				(size_t)ft_strlen(*stc));
		return (1);
	}
	else if (ft_strlen(*stc) > 0 && !(ft_strchr(*stc, '\n')))
	{
		*line = ft_strdup(*stc);
		ft_strdel(stc);
		return (1);
	}
	return (0);
}

int					get_next_line(const int fd, char **line)
{
	long int		rd;
	char			buffer[BUFF_SIZE + 1];
	static char		*stc[OPEN_MAX];

	if (fd < 0 || !line || BUFF_SIZE == 0)
		return (-1);
	rd = 0;
	if ((rd = read(fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[rd] = '\0';
		if (STC)
			STC = ft_strjoin_free(STC, buffer, 1);
		else
			STC = ft_strdup(buffer);
		if (!(ft_strchr(buffer, '\n')))
			return (get_next_line(fd, line));
	}
	if (((STC) && (ft_slice(line, &STC))) || (rd > 0))
		return (1);
	else if (rd <= 0 && (STC))
	{
		*line = ft_strdup(STC);
		ft_strdel(&STC);
	}
	return ((rd == -1) ? -1 : 0);
}
