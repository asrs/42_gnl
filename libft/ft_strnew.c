/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 15:04:52 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/10 21:33:31 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strnew(size_t size)
{
	char		*s;

	if (!(s = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	while (size > 0)
		*(s + size--) = '\0';
	*(s + size) = '\0';
	return (s);
}
