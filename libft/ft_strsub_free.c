/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub_free.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 15:17:25 by clrichar          #+#    #+#             */
/*   Updated: 2017/12/01 13:27:30 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char			*ft_strsub_free(char *s, unsigned int start, size_t len)
{
	size_t		i;
	char		*dest;

	if (!s)
		return (NULL);
	if (!(dest = ft_strnew(len)))
		return (NULL);
	i = 0;
	while (*(s + (start + i)) && i < len)
	{
		*(dest + i) = *(s + (start + i));
		i++;
	}
	free((void *)s);
	return (dest);
}
