/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/20 21:31:31 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/11 13:43:27 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	int			i;

	if (!(s && f))
		return ;
	i = 0;
	while (*s)
	{
		(*f)((unsigned int)i, s);
		s++;
		i++;
	}
}
