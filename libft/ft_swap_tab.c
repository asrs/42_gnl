/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 10:59:06 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/15 10:59:25 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			**ft_swap_tab(char **tab, int start, int end)
{
	char		*tmp;
	char		**ret;

	ret = tab;
	tmp = ret[start];
	ret[start] = ret[end];
	ret[end] = tmp;
	return (ret);
}
