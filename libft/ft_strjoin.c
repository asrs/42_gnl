/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 17:45:04 by clrichar          #+#    #+#             */
/*   Updated: 2017/12/01 11:00:14 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strjoin(const char *s1, const char *s2)
{
	char		*dest;

	if (!(s1 && s2))
		return (NULL);
	else if (!(dest = ft_strnew(ft_strlen(s1) + ft_strlen(s2))))
		return (NULL);
	if (s1)
		dest = ft_strcat(dest, s1);
	if (s2)
		dest = ft_strcat(dest, s2);
	return (dest);
}
