/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countword.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 19:58:01 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/11 16:49:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t					ft_countword(char const *s, char c)
{
	int					i;
	size_t				word;

	if (!s)
		return (-1);
	i = 0;
	word = 0;
	if (s[0] == c)
	{
		while (s[i] == c)
			i++;
	}
	else if (s[i])
		word = 1;
	while (s[i])
	{
		if (i > 1 && (s[i] != c && s[i - 1] == c))
			word++;
		i++;
	}
	return (word);
}
